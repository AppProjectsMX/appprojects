$(function(){
    $("[data-toggle='tooltip']").tooltip();
    $("[data-toggle='popover']").popover();
    $(".carousel").carousel({
        interval: 5000
    });

    $("#contacto").on('show.bs.modal', function (e){
        console.log('el modal se esta mostrando');

            $("#contactoA").removeClass('btn-outline-success');
            $("#contactoA").addClass('btn-primary');
            $("#contactoA").prop('disabled', true);
    });
    
    $("#contacto").on('shown.bs.modal', function (e){
        console.log('el modal se mostró');
    });

    $("#contacto").on('hide.bs.modal', function (e){
        console.log('el modal se oculta');
    });

    $("#contacto").on('hidden.bs.modal', function (e){
        console.log('el modal se ocultó');

            $("#contactoA").prop('disabled', false);
    });                
});